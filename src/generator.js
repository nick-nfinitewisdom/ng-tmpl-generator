/**
 * DISCLAIMER: Core logic was copied from https://github.com/karma-runner/karma-ng-html2js-preprocessor.
 * Since karma-ng-html2js-preprocessor was designed specifically for karma,
 * I modified it to create a standalone script.
 */

/* globals module */
'use strict';

var fs = require('fs');
var path = require('path');
var util = require('util');
var StringBuilder = require('stringbuilder')

// create an StringBuilder(); 


//
// Helper
//

var escapeContent = function(content) {
  return content.replace(/\\/g, '\\\\').replace(/'/g, '\\\'').replace(/\r?\n/g, '\\n\' +\n    \'');
};


//
// Main script
//

module.exports = function (templates, moduleName, filename) {

  var output = "angular.module("
  output +="'"+moduleName+"'"+")\r\n";
  output+=".run(['$templateCache', function($templateCache) {\r\n"
    for(let i=0; i< templates.length; i++){
        const template = templates[i];

        output+="$templateCache.put(";
        output+="'"+template.url+"'";
        output+=",";
        output+="'"+escapeContent(template.content)+"'";
        output+=");\r\n";
    
    }
  output+="}]);";

    fs.writeFileSync(filename, output, 'utf8');

};